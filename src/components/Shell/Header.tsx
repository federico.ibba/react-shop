import React from 'react'
import { Link } from 'react-router-dom'

import { makeStyles } from '@material-ui/core/styles'

import AppBar from '@material-ui/core/AppBar'
import Button from '@material-ui/core/Button'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'

import HomeIcon from '@material-ui/icons/Home'
import CartIcon from '@material-ui/icons/ShoppingCart'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    height: '7%',
  },
  menuButton: {
    color: 'white',
    marginRight: theme.spacing(2),
  },
  cartIcon: {
    color: '#ffc107',
  },
  title: {
    flexGrow: 1,
  },
}))

const Header: React.FC = () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar>
          <Link to='/'>
            <IconButton edge='start' color='inherit' aria-label='menu'>
              <HomeIcon className={classes.menuButton} />
            </IconButton>
          </Link>
          <Typography variant='h6' className={classes.title}>
            Yet Another React Shop
          </Typography>
          <Button color='inherit'>Login</Button>
          <Link to='/cart'>
            <IconButton>
              <CartIcon className={classes.cartIcon} />
            </IconButton>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default React.memo(Header)
