import React from "react";

export default interface IShell {
    children: React.ReactNode
}