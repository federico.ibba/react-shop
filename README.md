# Applicazione lista della spesa

- L’utente atterra su una pagina con un elenco di prodotto cliccabili ( per comodità useremo un faker per il BE ).
- A ogni click possiamo vedere il dettaglio prodotto ( + tasto indietro per tornare alla lista )
- Sia dalla lista che dal dettaglio posso aggiungere elementi a un carrello tramite button ‘Aggiungi al carrello’
- Previsto un header nella home page dove un bottone ‘Carrello’ mi farà accedere a tale sezione con l’elenco dei prodotti precedentemente cliccati, da comprare.
- Termina il tutto con un finto submit nella sezione ‘Carrello’ che azzera il carrello e rimanda alla home page con la lista dei prodotti.
 
## Requisiti di sviluppo

- Usare lo store e il pattern di Redux per la gestione del carrello ( meglio se gestiamo tutto il gestibile, quindi anche cose come lista prodotti e dettaglio )
- Applicativo storato su un git personale accedibile via link per condivisione di codice.
 
## Requisito Plus
- Authenticazione con Google account
- Testing con React testing di almeno le feature di store ( reducer selector e thunk middleware )
- Testing con Cypress di una funzionalità e2e. Esempio atterro sulla pagina clicco su un dettaglio e aggiungo al carrello quel prodotto
- Deploy su Netlify ( esempio )