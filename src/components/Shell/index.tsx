import React from 'react'

import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'

import IShell from 'types/shell'
import Header from './Header'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    display: 'flex',
    height: '93%',
    padding: '1.5rem',
  },
}))

const Shell: React.FC<IShell> = ({ children }) => {
  const classes = useStyles()

  return (
    <Grid className={classes.root}>
      <Header />
      <Box className={classes.content}>{children}</Box>
    </Grid>
  )
}

export default React.memo(Shell)
