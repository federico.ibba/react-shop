import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Shell from 'components/Shell'
import Cart from 'pages/cart'
import Products from 'pages/products'

import './index.scss'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Shell>
          <Route exact path='/' component={Products} />
          <Route exact path='/cart' component={Cart} />
          {/* <Route path='**' component={() => <span>Not found</span>} /> */}
        </Shell>
      </Switch>
    </BrowserRouter>
  )
}

export default App
